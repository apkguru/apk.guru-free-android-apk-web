Apk.guru - Free Android APK website

APK.Guru is one of the reputable websites that provides 10M+ APK files that you can download and install directly to your phone without accessing the Google Play store. 

We will help you to easily download region-restricted apps or apps that do not even exist on Google Play store. With apk.guru, you can freely choose any older version you want to install on your Android devices in the form of ZIP files. 

You can go straight to our website and search for the app name, or use Google to search for "your app name + apk.guru". Contact us if you need more support! 
Website: https://apk.guru/
Address: 21602 NE Sumner St STE Portland, Oregon, 97229, United State of America 
Email: apkdotguru@gmail.com 
Phone number: +1 818-877-2539 

Social media: 
Twitter: https://twitter.com/apkdotguru
Pinterest: https://www.pinterest.com/apkdotguru 
Flickr: https://www.flickr.com/people/190887957@N03/ 
Behance: https://www.behance.net/apkguru2 
Dribbble: https://dribbble.com/dotguru/about 
Flipboard: https://flipboard.com/@apkguru 
Scoop.it: https://www.scoop.it/u/apkguru 
Gab: https://gab.com/apkguru 
Ello: https://ello.co/apkdotguru 
Mastodon: https://mastodon.social/@apkguru 
Plurk: https://www.plurk.com/apkdotguru
